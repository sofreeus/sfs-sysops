TODO:

- [x] Create 5.5T RAID1 from unconfigured disks
- [x] Create pv, extend vg, remove RAID0 pv
- [x] Remove all RAID0 vols
- [x] Physically remove unconfigured disks
- [x] Upgrade physical media in Neo
- [ ] Downgrade physical media in Owl
- [ ] Buy four 2.5" to 3.5" adapters
- [ ] Install 4 SSDs using adapters
- [cancel] After getting a good backup of Nextcloud on to a separate volume, combine the old backup disk and the single 3.7T into a 3.7T RAID1

    ---

```bash
ps aux | grep 'kvm -id ' | cut -c 60-200  | cut -d ' ' -f 5- | cut -f 1 -d , | sort -n
```

```
100 -name happy.sofree.us
101 -name lists.sofree.us
105 -name blue.sofree.us
107 -name civiCRM
108 -name files
109 -name gar-bitwarden
110 -name Nextcloud22
112 -name GLAM2-MattermostViaGLAuth
113 -name moodle
115 -name k0s-w1.int.sofree.us
116 -name k0s-c1.int.sofree.us
117 -name k0s-c2.int.sofree.us
118 -name k0s-w2.int.sofree.us
119 -name k0s-w3.int.sofree.us
371 -name OPNsense
373 -name Docker
375 -name Twitch
377 -name test
379 -name ZFSmox
999 -name BBT
2000 -name briefcase
2001 -name thepromisedland
2002 -name speedtest
2003 -name LightMage670
2004 -name connect
2005 -name bear-metal
2007 -name k8s-dlw-orange
2008 -name k8s-dlw-green
2009 -name k8s-dlw-purple
2010 -name HaiHaiHayley
```

Shut the guests down:

```bash
ps aux |
    grep '[k]vm -id ' |
    cut -d / -f 4-  |
    cut -d ' ' -f 3 |
    sort -rn |
    while read VMID
    do
        set -x
        sudo qm shutdown $VMID --timeout 64 --forceStop 1
    done
```

Michael: rocky-9-cloud (9010) was stopped but set to auto-start. Assuming that it was stopped on purpose, I removed the auto-start.

Jon: happy2 (6001) was stopped but set to auto-start. Assuming that it was stopped on purpose, I removed the auto-start.

- shut down
- destroy RAID0 volumes
- pop out single/unmatched disks
- swap 6T into neo
- power up