# Rotate the Neo backup disk

Disable neo-backups in [Proxmox](https://proxmox.sofree.us/) -> Datacenter(ClusterN) -> Storage -> neo-backups

```bash
# Unmount from all Proxmox hosts
ansible dwarven_proxmox_hosts -a "umount -lf /mnt/pve/neo-backups" -b
# Verify that it stayed unmounted by re-running that command
```

Patch and Shutdown Neo

```bash
ssh neo.int.sofree.us
sudo apt update && sudo apt -y full-upgrade && sudo apt -y autoremove
# disable backups vol in /etc/fstab
sudo vim /etc/fstab
# stop and disable nfs-server
sudo systemctl stop nfs-server.service
sudo systemctl disable nfs-server.service
# poweroff Neo
sudo poweroff
```

1. Disconnect Neo power
2. Rotate disk in sled
3. Connect Neo power

```bash
# verify the new volume:
sudo blkid /dev/sda
# expect something like /dev/sda: LABEL="backup-vol-c"
# enable backups vol in /etc/fstab
sudo vim /etc/fstab
# mount /srv/backups
sudo mount -av
# start and enable nfs-server
sudo systemctl start nfs-server.service
sudo systemctl enable nfs-server.service
```

Enable neo-backups in [Proxmox](https://proxmox.sofree.us/) -> Datacenter(ClusterN) -> Storage -> neo-backups

```bash
# Verify that backup volumes come back up on all Proxmox nodes
ansible dwarven_proxmox_hosts -a "ls -l /mnt/pve/neo-backups/" -b
# Destroy really, really old backups
# ssh to Neo
ssh neo.int.sofree.us
# drop files older than 6 months
sudo find /srv/backups/ -xtype f -mtime +180 -delete -ls
# drop empty directories
sudo find /srv/backups/ -xtype d -empty -delete -ls
```