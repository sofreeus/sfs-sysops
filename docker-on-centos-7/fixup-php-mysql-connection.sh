#!/bin/bash -eu
cd "$( dirname "$0")"
namespace="devops"

# The .env file must exist
# TODO: build the .env file with an Ansible playbook, template and variables, and skip this whole hack
ls code/.env > /dev/null

# The network must exist
docker network inspect ${namespace}_control-panel-net > /dev/null

# The database container must exist
docker inspect ${namespace}_mysql_1 > /dev/null

# The php container must exist
docker inspect ${namespace}_php_1 > /dev/null

# copy MYSQL_RANDOM_ROOT_PASSWORD to .env
echo "Waiting for the MySQL root password..."
rootpw='unknown'
while [[ $rootpw == 'unknown' ]]
do
    rootpw=$( docker logs ${namespace}_mysql_1 2>/dev/null |
        grep "GENERATED ROOT PASSWORD" | awk '{ print $4}')
    if [[ $( expr length "$rootpw" ) == 32 ]]
    then
        echo 'got it!'
    else
        rootpw='unknown'
        echo -n .
        sleep 1
    fi
done

echo "Implanting the MySQL root password into the .env file..."
# echo "DEBUG: rootpw=$rootpw"
sudo sed -e 's/^DB_PASSWORD=.*$/DB_PASSWORD='"$rootpw"'/' -i code/.env

# add MySQL support to php
docker exec ${namespace}_php_1 docker-php-ext-install pdo_mysql

# restart php
docker restart ${namespace}_php_1

# prompt for interactives
echo "-----------------------------------------------------"
echo "You may want to run Artisan commands, like:"
echo "php artisan migrate:[re]fresh --seed"
echo "If you do, be sure to `cd /code` first"
echo "To get an interactive shell in the php container, run:"
echo "docker exec -it devops_php_1 /usr/bin/env bash"
echo "-----------------------------------------------------"
