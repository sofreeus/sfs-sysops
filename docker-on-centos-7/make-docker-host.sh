#!/usr/bin/env bash
set -eu
cd "$( dirname "$0" )"
. rc

ansible-playbook docker-host.yml --extra-vars="TARGET=$1" -K --ask-vault-pass
