- [ ] Set the key creator so that it grabs ~/.ssh/id_rsa.pub
- [ ] Destroy and re-create the build key every time
- [ ] Automate addition of non-default admins
- [ ] Automate removal of default admins
- [ ] add creation of SG for mail-servers
- [ ] add creation of SG for managed-nodes
- [ ] fix the mail-omnibus security group
- [ ] Skip removing the default admin when the default admin is doing the run
     "ansible_user_id": "ubuntu"
