# SFS SysOps

This is where all the things around SFS' IT systems go.

******

Ansible info available in the Ansible folder. Jumphost info is in ssh.config.

Monitoring, Alerting, and Dashboards with Prometheus, Grafana, and friends:
- [Ours](mad-pgaf/)
- [The Class](https://gitlab.com/sofreeus/mad-pgaf/)

---

```bash
# Git submodules enabled for ansible roles.  When you clone the repo:
git clone --recurse-submodules --remote-submodules https://gitlab.com/sofreeus/sfs-sysops.git

# To add submodules to an existing clone
git submodule update --init

# To update submodules in an already cloned repo:
git submodule update --remote
```
