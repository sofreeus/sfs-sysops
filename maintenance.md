# Maintenance

Maintenance should not run on or right after [Patch Tuesday](https://en.wikipedia.org/wiki/Patch_Tuesday).

Mondays seem like good days to do maintenance.

## Quarterly

Review templates in Proxmox
* 2024-12-00 Update template (rocky9)
* 2025-02-00 Update template (debian)
* 0000-00-00 Update template (fedora)


## Monthly

2025-02-17 Ensure that the next maintenance mob is scheduled in the current tool (ex. Meetup.com or other)

2025-02-17 SysOps - Cleanup merge requests and branches

2025-02-17 Cleanup Kanban board
- Review recently Closed issues
  Is there anything to share or are any follow-up tickets needed?
- Review all Doing - Is it done? Is it blocked? What's needed?
- Review all ToDo - Is is needed/wanted soon? Is it ready to work? Is this column prioritized?
- Review all Backlog - If we're never going to do it, toss it.

2025-02-17 Patch and reboot everything

```bash
# Patch up a Fedora/CentOS/Alma/RHEL/Rocky machine
type dnf && sudo dnf -y upgrade
# Patch up a Debian/Ubuntu machine
type apt && sudo apt update && sudo apt -y autoremove && sudo apt -y full-upgrade && sudo apt -y autoremove
```

Before rebooting any Proxmox node, record which machines are running and shut them down or move them to other hosts.

```bash
cd git/sfs-sysops/ansible/
ansible dwarven_proxmox_hosts -a "/usr/sbin/qm list" -b
```

[Machines](Machines.ods)

0000-00-00 Check all services for updates

- [nocheck] Nextcloud
- [nocheck] Zimbra #100
- [noupdate] Mailman on Lists #103
- [01/2025] bitwarden
- [02/14/2025-noupdate] Mattermost
- [broken] BigBlueButton ; 02/14/2025 Richard Calvo has been contacted per DLW
- [02/17/2025-updated] Proxmox
- [02/01/2025-updated] QNAP eth0:73.19 eth1:73.16 eth2:x eth3:73.20

2025-02-17 [Rotate the Neo backup disk](rotate-backup-disk.md)
