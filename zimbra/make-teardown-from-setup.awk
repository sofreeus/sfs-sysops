# tear down distribution lists
/createDistributionList/ {print "deleteDistributionList " $2}
# remove users
/createAccount/ {print "deleteAccount " $2}
# remove domain
/createDomain/  {print "deleteDomain " $2}
