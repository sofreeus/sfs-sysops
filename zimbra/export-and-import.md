Using the UI works just fine. Go to the Preferences tab > Import/Export.

If you'd rathe do from the command line, read on...

Export (run this on the old server):

```bash
mailbox=dlwillson@thegeek.nu
zmmailbox -z -m $mailbox getRestURL "//?fmt=tgz" > /shared/$mailbox-$( date --iso ).tgz
```

Import (run this on the new server):

```bash
mailbox=dlwillson@thegeek.nu
# merge, replacing duplicates
zmmailbox -z -m $mailbox postRestURL "//?fmt=tgz&resolve=replace" /shared/$mailbox-$( date --iso ).tgz
# reset the target mailbox
# zmmailbox -z -m $mailbox postRestURL "//?fmt=tgz&resolve=reset" /shared/$mailbox-$( date --iso ).tgz

```

Shared storage is not strictly necessary. Files can be copied into place, instead.

See also:
- https://virtualimpressions.com/zimbra-account-export-import-from-command-line/
- https://wiki.zimbra.com/wiki/Zimbra_REST_API_Reference
