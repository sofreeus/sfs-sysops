# Cloud image to virtual machine template steps!
-----

Source Link: https://austinsnerdythings.com/2021/08/30/how-to-create-a-proxmox-ubuntu-cloud-init-image/
Cloud Images: https://cloud-images.ubuntu.com/
-----

## Steps to download the cloud image:
1. Run sshuttle
2. SSH into pm.int.sofree.us
3. Choose your cloud image on the link above
4. From this link, you'll want to click on the distro (i.e. focal, jammy, etc.)
5. Then, click current
6. Then, copy the link of the file that reads (distro)-server-cloudimg-amd64.img
7. Run this command: wget (link)
8. Set the downloaded file's name as a variable called (template_filename) or something

## Steps to configure the cloud image
1. Run this command: sudo apt update -y && sudo apt install libguestfs-tools -y
2. Run this command: sudo virt-customize -a (distro)-server-cloudimg-amd64.img --install qemu-guest-agent

## Steps to make a machine off of it, and turn it into a template
1. Choose an ID number, make it a variable
2. In pm, run the following commands
```
sudo qm create (id_number) --name "(insert vm name here)" --memory (insert ram in mb) --cores (insert core number, recommend 2) --net0 virtio,bridge=vmbr0
sudo qm importdisk (id_number) (template_filename) local-lvm
sudo qm set (id_number) --scsihw virtio-scsi-pci --scsi0 local-lvm:vm-(id_number)-disk-0
sudo qm set (id_number) --boot c --bootdisk scsi0
sudo qm set (id_number) --ide2 local-lvm:cloudinit
sudo qm set (id_number) --serial0 socket --vga serial0
sudo qm set (id_number) --agent enabled=1
sudo qm template (id_number)
```

You should now have a template!
