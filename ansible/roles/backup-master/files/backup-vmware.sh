#!/bin/bash -eu
echo "$0:begin:$( date )"
sudo umount /mnt/ || true
sudo mount LABEL=backups /mnt/

eval $( ssh-agent )
ssh-add $HOME/.ssh/backup.key

# ip a | grep 192.168.73 || sudo ip addr add 192.168.73.33/24 dev enp3s0

echo "
umount /mnt/ || true
lvremove --force vmware-vg/machines-backup || true
lvcreate --snapshot vmware-vg/machines --extents 50%FREE --name machines-backup
mount /dev/vmware-vg/machines-backup /mnt
" | ssh -q root@vmware.int.sofree.us

sudo chown $USER /mnt/
rsync -avH --delete root@vmware.int.sofree.us:/mnt/ /mnt/machines/

echo "
umount /mnt/
lvremove --force vmware-vg/machines-backup
" | ssh -q root@vmware.int.sofree.us

echo "
umount /mnt/ || true
lvremove --force vmware-vg/vmware-backup || true
lvcreate --snapshot vmware-vg/vmware --extents 50%FREE --name vmware-backup
mount /dev/vmware-vg/vmware-backup /mnt
" | ssh -q root@vmware.int.sofree.us

rsync -avH --delete root@vmware.int.sofree.us:/mnt/ /mnt/vmware/

echo "
umount /mnt/
lvremove --force vmware-vg/vmware-backup
"  | ssh -q root@vmware.int.sofree.us

# sudo ip addr del 192.168.73.33/24 dev enp3s0

sudo umount /mnt/
echo "$0:finis:$( date )"
